from django import forms
from app.models import Question, Author
from app.models import Tag
from django.utils import timezone
from django.forms import ModelForm

class AskForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ['title', 'content', 'tags']

    def __init__(self, author, *args, **kwargs):
        self.author = author
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        q = Question.objects.create(
                        rating=0,
                        author=self.author,
                        title=self.data.get('title'),
                        content=self.data.get('content'),
                        date_added=timezone.now(),
                    )

        tags = self.data.get('tags').split(' ')
        for tag_name in tags:
            try:
                t=Tag.objects.get(title=tag_name)
                t.rating = t.rating + 1
                t.save()
            except:
                t=Tag.objects.create(title=tag_name, rating = 1)
            q.tags.add(t)
        if commit:
            q.save()
        return q

    def clean_title(self):
        data = self.cleaned_data['title']
        if 'bad word' in data:
            self.add_error('title', 'bad word detected!')
        return data

    def is_valid(self):
        data_title = self.data.get('title')
        data_content = self.data.get('content')
        data_tags = self.data.get('tags')

        if 'bad word' in data:
            self.add_error('title', 'bad word detected!')
            return False
        return False



class AuthorForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = ['avatar']


    #def save(self, commit=True):
    #    a = Author.objects.create(
    #                    user=self.instance.user,
    #                    avatar=self.data.get('avatar'),
    #                )
    #    author.avatar = self.data.avatar
    #
    #    if commit:
    #        author.save()
    #    return q
